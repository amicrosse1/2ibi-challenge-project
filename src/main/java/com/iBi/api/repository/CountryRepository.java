package com.iBi.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.iBi.api.model.Country;

public interface CountryRepository extends JpaRepository<Country, Long>{

	@Query("SELECT c FROM Country c WHERE c.name=?1")
	public List<Country> findCountryByName(String name);
	
	@Query("SELECT c FROM Country c WHERE c.capital=?1")	
	public List<Country> findCapitalByName(String capital);
}
