package com.iBi.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name = "Country")
@Table(name = "Country")
public class Country {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", nullable = false, unique = true)
	private String name;
	
	@Column(name = "capital", nullable = false, unique = true)
	private String capital;
	
	@Column(name = "region", nullable = false, unique = false)
	private String region;
	
	@Column(name = "sub_region", nullable = false, unique = false)
	private String sub_region;
	
	@Column(name = "area", nullable = false)
	private String area;

	//Por time stamp de carregamento
	//Por time stamp de actualizacao
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSub_region() {
		return sub_region;
	}

	public void setSub_region(String sub_region) {
		this.sub_region = sub_region;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	
}
