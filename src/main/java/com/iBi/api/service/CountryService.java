package com.iBi.api.service;

import java.util.List;

import org.springframework.data.domain.Sort.Order;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import com.iBi.api.model.Country;

@Component
public interface CountryService {
	@Secured ({"ROLE_ADMIN"})
	Country saveCountry(Country country);
	
	@Secured ({"ROLE_ADMIN", "ROLE_USER"})
	List<Country> getAll(List<Order> orders);
	
	@Secured ({"ROLE_ADMIN"})
	void removeCountryById(Long id);
	
	@Secured ({"ROLE_ADMIN", "ROLE_USER"})
	Country findCountrybyId(Long id);
	
	@Secured ({"ROLE_ADMIN"})
	Country updateCountry(Country country, Long id);
	
	boolean findCountryByName(String name);
	boolean findCapitalByName(String name);
}
