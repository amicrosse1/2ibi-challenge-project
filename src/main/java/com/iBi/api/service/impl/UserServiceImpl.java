package com.iBi.api.service.impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.iBi.api.model.User;
import com.iBi.api.model.UserPrincipal;
import com.iBi.api.repository.UserRepository;

@Service
public class UserServiceImpl implements UserDetailsService{


	@Autowired
	private UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User _user = this.repository.findByUsername(username);
		GrantedAuthority authority;
		if(_user == null)
			throw new UsernameNotFoundException("Error 404: User not Found!!! ");
		else {
			 authority = new SimpleGrantedAuthority(_user.getRole());
		}
		return new UserPrincipal(_user, Arrays.asList(authority));
	}
}
