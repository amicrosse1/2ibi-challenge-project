package com.iBi.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.iBi.api.exception.handling.ResourceNotFound;
import com.iBi.api.model.Country;
import com.iBi.api.repository.CountryRepository;
import com.iBi.api.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService{
	
	@Autowired
	private CountryRepository repository;
	
	@Override
	public Country saveCountry(Country country) {
		return repository.save(country);
	}

	@Override
	public List<Country> getAll(List<Order> orders) {
		return repository.findAll(Sort.by(orders));
	}

	@Override
	public void removeCountryById(Long id) {
		 repository.delete(findCountrybyId(id));
	}

	public Country findCountrybyId(Long id) {	
		Optional<Country> country = null;
		country = repository.findById(id);
		if(country.isPresent())
			return country.get();
		else
			return null;
	}

	@Override
	public Country updateCountry(Country country, Long id) {
		
		//Check if country exists
		Country _country = repository.findById(id).orElseThrow(() -> new ResourceNotFound("Country", "ID", id));

		_country.setName(country.getName());
		_country.setCapital(country.getCapital());
		_country.setRegion(country.getRegion());
		_country.setSub_region(country.getSub_region());
		_country.setArea(country.getArea());
		
		//Update into Data base
		repository.save(_country);

		return _country;
	}

	@Override
	public boolean findCountryByName(String name) {
		if(repository.findCountryByName(name).size()>0)
			return true;
		else return false;
	}

	@Override
	public boolean findCapitalByName(String name) {
		if(repository.findCapitalByName(name).size()>0)
			return true;
		else return false;
	}

}
