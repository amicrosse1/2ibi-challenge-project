package com.iBi.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iBi.api.model.Country;
import com.iBi.api.service.CountryService;
import com.iBi.api.util.ErrorDisplay;

@RestController
@CrossOrigin(origins = "https://com-2ibi.herokuapp.com")
@RequestMapping("/api")
public class CountryController {

	@Autowired
	private CountryService countryService;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")) {
	      return Sort.Direction.DESC;
	    }

	    return Sort.Direction.ASC;
	  }

	// Building add Country REST API
	@RequestMapping(value = "/saveCountry", method = RequestMethod.POST)
	public ResponseEntity<?> saveCountry(@RequestBody Country country) {
		
		try {
		//Validate if exists same country
		if(countryService.findCountryByName(country.getName()))
			return new ResponseEntity<ErrorDisplay>(new ErrorDisplay("Country with same name alread exist", "400"), HttpStatus.BAD_REQUEST);
		
		//Validate if exists same capital		
		if(countryService.findCapitalByName(country.getCapital()))
			return new ResponseEntity<ErrorDisplay>(new ErrorDisplay("Country with same capital alread exist", "400"), HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<Country>(countryService.saveCountry(country), HttpStatus.CREATED);
	
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//Get all Countries and order by default by ID Desc
	@RequestMapping(value = "/getAllCountries", method = RequestMethod.GET)
	public ResponseEntity<List<Country>> getAllCountries(@RequestParam(defaultValue = "id,desc") String[] sort){		

		try {
			List<Order> orders = new ArrayList<Order>();
			orders.add(new Order(getSortDirection(sort[1]), sort[0]));
			List<Country> list = countryService.getAll(orders);
			
			if(list.isEmpty())
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);

			return new ResponseEntity<List<Country>>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	  @DeleteMapping("/delete/country/{id}")
	  public ResponseEntity<?> deleteCountries(@PathVariable("id") long id) {
	    try {
	    	if(countryService.findCountrybyId(id)==null) {
	    		return new ResponseEntity<ErrorDisplay>(new ErrorDisplay("Not Found", "404"), HttpStatus.NOT_FOUND);
	    	}else {
	    		countryService.removeCountryById(id);
	      return new ResponseEntity<ErrorDisplay>(new ErrorDisplay("Success", "200"), HttpStatus.OK);
	      }
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }

	  @PutMapping("/update/country/{id}")
	  public ResponseEntity<?> updateCountry(@PathVariable("id") long id, @RequestBody Country country){
		  
		  try {
				/*if(countryService.findCountrybyId(id)!=null) {
			  	
			  	//Validate if exists same country
				if(countryService.findCountryByName(country.getName()))
					return new ResponseEntity<String>("THe country with the same name already exist!", HttpStatus.BAD_REQUEST);
				
				//Validate if exists same capital		
				if(countryService.findCapitalByName(country.getCapital()))
					return new ResponseEntity<String>("THe country with the same capital already exist!", HttpStatus.BAD_REQUEST);
				}*/
			  	if(countryService.findCountrybyId(id) != null)
			  		return new ResponseEntity<Country>(countryService.updateCountry(country, id), HttpStatus.OK);
			  	else
			  		return new ResponseEntity<ErrorDisplay>(new ErrorDisplay("Not Found", "404"), HttpStatus.NOT_FOUND);
		  }catch(Exception e) {
			  return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	  }
	
}
