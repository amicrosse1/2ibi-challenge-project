package com.iBi.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		   BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		    String password [] = {"letmein", "test", "2ibi"};
		    for(int i = 0; i < password.length; i++)
		        System.out.println(passwordEncoder.encode(password[i]));		    		
	}

}
