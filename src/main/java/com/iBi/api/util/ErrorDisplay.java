package com.iBi.api.util;

public class ErrorDisplay {

	private String message;
	private String code;
	
	
	public ErrorDisplay(String message, String code) {
		super();
		this.message = message;
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
