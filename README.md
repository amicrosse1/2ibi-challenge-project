# 2iBi Challenge Project
Este projecto tem que como objectivo desenvolver RESTfull API dado como desafio pela [2iBi](https://2ibi.com/carreiras/back-end-engineer/) para a vaga de Backend Engineer.

Para a implementação do projecto foram usadas as seguintes tecnologia
* Spring Boot
* PostgreSQL
* [Heroku](https://com-2ibi.herokuapp.com/) para hospedagem da app
* [Gitlab](www.gitlab.com) para repositório 
* [Postman](https://www.postman.com/downloads/) ou [SoapUI](https://www.soapui.org/downloads/soapui/) para testar os endpoits

## Descrição
O projecto consiste nos seguintes RESTFul API
<dl>
  <dt>Criar um novo pais</dt>
  <dd><mark>API: https://com-2ibi.herokuapp.com/api/saveCountry</mark></dd>
  <dt>Listar todos os paises e ordernar por qualquer uma das suas propriedades</dt>
  <dd><mark>API: https://com-2ibi.herokuapp.com/api/getAllCountries?sort={column_name},{asc|desc}      </mark></dd>
  <dt>Modificar os dados de um pais anteriorimente criado</dt>
  <dd><mark>API: https://com-2ibi.herokuapp.com/api/update/country/{id}</mark></dd>
  <dt>Eliminar um pais anteriorimente criado</dt>
  <dd><mark>API: https://com-2ibi.herokuapp.com/api/delete/country/{id}</mark></dd>
</dl>

De modo a usar qualquer endpoits acima mencionado foi implementando **SPRING SECURITY** para garantir a segurança.

## Endpoints Security Credentials
* **Username:** admin
* **Password:** letmein

## SQL Tables Schema
A base de dados contem apenas duas tabelas: **COUNTRY** e **USER_LOGIN** e com as seguintes estruturas:\
**Table COUNTRY**
| column_name     | data_type         |
|------------|-------------------|
| id         | bigint            |
| area       | character varying |
| capital    | character varying |
| name       | character varying |
| region     | character varying |
| sub_region | character varying |

\
**Table USER_LOGIN**
| column_name | data_type         |
|-------------|-------------------|
| id          | bigint            |
| passwd      | character varying |
| role        | character varying |
| username    | character varying |


## Teste dos Endpoints usando Postman
Segue abaixo os testes e os resultados para cada Endpoits

* **API:**  https://com-2ibi.herokuapp.com/api/saveCountry
![](gif/api_add_country.gif)

* **API:**  https://com-2ibi.herokuapp.com/api/getAllCountries
![](gif/api_get_sort.gif)

* **API:**  https://com-2ibi.herokuapp.com/api/update/country/{id}
![](gif/api_update_country.gif)

* **API:**  https://com-2ibi.herokuapp.com/api/delete/country/{id}
![](gif/api_delete_country.gif)



